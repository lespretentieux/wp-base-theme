<?php

/**
 * Defining global constants
 */
define('THEME_VERSION',          '1.0.0');
define('THEME_URL',              get_stylesheet_directory_uri());
define('THEME_TEMPLATE_URL',     get_template_directory_uri());
define('THEME_PATH',             dirname(__FILE__) . '/');
define('THEME_INC',              THEME_PATH . 'includes/');
define('THEME_ASSETS',           THEME_TEMPLATE_URL . '/assets/');
define('TEXT_DOMAIN',            'Les Prétentieux');
define('ACTIVATE_PRX_MARQUEE',   true);
define('ACTIVATE_PRX_SCROLL', true);
define('ACTIVATE_FLICKITY',      true);

// Loads composer autoload if found in theme folder
if (file_exists($composer = __DIR__ . '/vendor/autoload.php')) {
    require $composer;
}

require_once THEME_INC . 'lib/custom-post-types.php';

require_once THEME_INC . 'core.php';
require_once THEME_INC . 'menus.php';
require_once THEME_INC . 'post-types.php';
require_once THEME_INC . 'plugins.php';
require_once THEME_INC . 'comments.php';
require_once THEME_INC . 'cleaner.php';
require_once THEME_INC . 'helpers.php';
require_once THEME_INC . 'editor.php';

THEME_WP\Core\setup();
THEME_WP\Menus\setup();
THEME_WP\Post_Types\setup();
THEME_WP\Plugins\setup();
THEME_WP\Comments\setup();
THEME_WP\Cleaner\setup();
THEME_WP\Editor\setup();
