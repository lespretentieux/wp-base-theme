/*
 * Import librairies
 */
import { PrxMarquee } from "../../../libraries/prx-marquee/prx-marquee";
import { PrxScroll } from "../../../libraries/prx-scroll/prx-scroll";

/*
 * Import modules
 */
import accordion from "./modules/accordion";
import carousel from "./modules/carousel";
import cf7 from "./modules/cf7";
import cursor from "./modules/cursor";
import file from "./modules/file";
import helper from "./modules/helper";

/*
 * Init librairies
 */

// Prx Marquee
var marqueeInstance;

if (options.activate_prx_marquee) {
	marqueeInstance = new PrxMarquee();
}

export const marquee = marqueeInstance;

// Prx Scroll
var scrollInstance;

if (options.activate_prx_scroll) {
	scrollInstance = new PrxScroll();
}

export const scroll = scrollInstance;

/*
 * Init modules
 */

accordion();
if (options.activate_flickity) {
	carousel();
}
cf7();
cursor();
file();
helper();
