export default function () {
	var $ = jQuery;

	var timeout;
	var $wpcf7Forms = $(".wpcf7-form");

	function callback(mutationsList, observer) {
		mutationsList.forEach((mutation) => {
			if (mutation.attributeName === "class") {
				clearTimeout(timeout);
				$(mutation.target).find(".wpcf7-response-output").addClass("is-active");

				timeout = setTimeout(function () {
					$(mutation.target).find(".wpcf7-response-output").removeClass("is-active");
				}, 5000);

				if (mutation.target.className == "wpcf7-form submitting") {
					if ($("html").attr("lang") == "fr") {
						$(mutation.target).find(".wpcf7-response-output").text("Envoi...");
					} else {
						$(mutation.target).find(".wpcf7-response-output").text("Sending...");
					}
				}
			}
		});
	}

	const mutationObserver = new MutationObserver(callback);

	$wpcf7Forms.each(function () {
		mutationObserver.observe(this, { attributes: true, attributeOldValue: true });
	});
}
