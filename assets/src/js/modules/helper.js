export default function () {
	var $ = jQuery;

	var lastScrollTop = 0;

	$(window).on("scroll", function () {
		var scrollY = window.pageYOffset;

		if (scrollY > lastScrollTop) {
			$("html").removeClass("is-scroll-down");
		} else if (scrollY < lastScrollTop) {
			$("html").removeClass("is-scroll-up");
		}

		if (scrollY > 160) {
			$("html").addClass("is-over-content");
		} else {
			$("html").removeClass("is-over-content");
		}

		lastScrollTop = window.pageYOffset;
	});
}
