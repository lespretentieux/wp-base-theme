export default function () {
	var $ = jQuery;

	// The lerp easing (how smoothly the cursor follows the mouse pointer)
	var easing = 0.35;
	var $cursor = $(".c-cursor");

	var mouseX = 0;
	var mouseY = 0;

	var cursorTranslateX = 0;
	var cursorTranslateY = 0;

	window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

	requestAnimationFrame(animate);

	$(".o-carousel").on("dragMove.flickity", function (event) {
		mouseX = event.clientX;
		mouseY = event.clientY;
	});

	$(window).mousemove(function (event) {
		mouseX = event.clientX;
		mouseY = event.clientY;
	});

	function animate() {
		var cursorWidth = $cursor.outerWidth();
		var cursorHeight = $cursor.outerHeight();

		cursorTranslateX = lerp(cursorTranslateX, mouseX, easing);
		cursorTranslateY = lerp(cursorTranslateY, mouseY, easing);

		$cursor.css("transform", "translate3d(" + (cursorTranslateX - cursorWidth / 2) + "px, " + (cursorTranslateY - cursorHeight / 2) + "px, 0)");

		requestAnimationFrame(animate);
	}

	function lerp(start, end, amt) {
		return (1 - amt) * start + amt * end;
	}

	/**
     * To change the cursor's state depending on what is being hovered
	$(document).on(
		{
			mouseover: function (event) {
				event.stopPropagation();
				$(".c-cursor").addClass("is-pointer");
			},
			mouseout: function (event) {
				event.stopPropagation();
				$(".c-cursor").removeClass("is-pointer");
			},
		},
		".js-cursor-is-pointer"
	); 
    */
}
