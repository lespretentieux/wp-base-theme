export default function () {
	var $ = jQuery;

	$(".c-file input").change(function () {
		var $file = $(this).closest(".c-file");
		var filename = $(this)
			.val()
			.replace(/C:\\fakepath\\/i, "");
		var filesize = jQuery(this)[0].files[0].size;

		$file.find(".c-file_info").remove();

		$file.append('<div class="c-file_document"><div class="c-file_meta"><span class="c-file_name">' + filename + '</span><span class="c-file_size">' + Math.ceil(filesize / 1000) + 'ko</span></div><span class="c-file_clear"></span></div>');
	});

	$(document).on("click", ".c-file_clear", function () {
		var $file = $(this).closest(".c-file");

		$file.find(".c-file_document").remove();
		$file.find("input").val("");
	});
}
