const gulp = require("gulp");
const uglify = require("gulp-uglify");
const cssnano = require("gulp-cssnano");
const autoprefixer = require("gulp-autoprefixer");
const sass = require("gulp-dart-sass");
const imagemin = require("gulp-imagemin");
const sourcemaps = require("gulp-sourcemaps");
const webpack = require("webpack-stream");
const babel = require("gulp-babel");
const mode = require("gulp-mode")();
const sassGlob = require("gulp-sass-glob");
const svgSprite = require("gulp-svg-sprite");
const Fiber = require("fibers");

sass.compiler = require("sass");

function styles() {
	return gulp
		.src("assets/src/scss/main.scss")
		.pipe(gulp.src("assets/src/scss/admin.scss"))
		.pipe(mode.development(sourcemaps.init()))
		.pipe(sassGlob())
		.pipe(
			sass({
				includePaths: ["./node_modules"],
				fiber: Fiber,
			}).on("error", sass.logError)
		)
		.pipe(autoprefixer())
		.pipe(cssnano())
		.pipe(mode.development(sourcemaps.write()))
		.pipe(gulp.dest("assets/dist/css"));
}

function scripts() {
	return gulp
		.src("assets/src/js/main.js")
		.pipe(
			webpack({
				mode: mode.development() ? "development" : "production",
				watch: true,
				output: {
					filename: "bundle.js",
				},
			})
		)
		.pipe(babel({ presets: ["@babel/preset-env"] }))
		.pipe(mode.development(sourcemaps.init()))
		.pipe(
			uglify().on("error", (uglify) => {
				console.error(uglify.message);
				this.emit("end");
			})
		)
		.pipe(mode.development(sourcemaps.write()))
		.pipe(gulp.dest("assets/dist/js"));
}

function images() {
	return gulp.src("assets/src/images/*").pipe(mode.production(imagemin())).pipe(gulp.dest("assets/dist/images/"));
}

function fonts() {
	return gulp.src(["assets/src/fonts/*"]).pipe(gulp.dest("assets/dist/fonts/"));
}

function icons() {
	return gulp
		.src(["assets/src/icons/*"])
		.pipe(
			svgSprite({
				shape: {
					dimension: {
						// Set maximum dimensions (used as a perfect square ratio here)
						maxWidth: 1,
						maxHeight: 1,
					},
				},
				mode: {
					stack: true,
					css: {
						render: {
							scss: {
								dest: "../../../src/scss/sprite.scss",
								template: "assets/src/scss/sprite-template.scss",
							},
						},
					},
				},
				variables: {
					mapname: "icons",
				},
			})
		)
		.pipe(gulp.dest("assets/dist/icons/"));
}

function watchFiles() {
	gulp.watch("assets/src/**/*.scss", gulp.series(styles));
	gulp.watch("assets/src/**/*.js", gulp.series(scripts));
	gulp.watch("assets/src/images/**/*.*", gulp.series(images));
	gulp.watch("assets/src/fonts/**/*.*", gulp.series(fonts));
	gulp.watch("assets/src/icons/**/*.*", gulp.series(icons));
	return;
}

exports.styles = styles;
exports.scripts = scripts;
exports.images = images;
exports.fonts = fonts;
exports.icons = icons;
exports.watch = gulp.parallel(styles, scripts, images, fonts, icons, watchFiles);
exports.default = gulp.parallel(styles, scripts, images, fonts, icons);
