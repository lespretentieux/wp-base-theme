<?php

namespace THEME_WP\Core;

/**
 * Set up theme defaults and register supported WordPress features.
 *
 * @return void
 */
function setup()
{
	$n = function ($function) {
		return __NAMESPACE__ . "\\$function";
	};

	add_action('wp_enqueue_scripts', $n('enqueue_scripts'));
	add_action('wp_enqueue_scripts', $n('enqueue_styles'));
	add_action('admin_enqueue_scripts', $n('enqueue_admin_styles'), true);
	add_action('after_setup_theme', $n('after_setup_theme'));

	add_filter('show_admin_bar', '__return_false');

	add_action('acf/init', $n('base_acf_options_pages'));
}

/**
 * Enqueue a custom stylesheet in the WordPress admin.
 * @param bool $debug Whether to enable loading uncompressed/debugging assets. Default false.
 * @return void
 */
function enqueue_admin_styles()
{
	wp_enqueue_style('admin', THEME_URL . "/assets/dist/css/admin.css", array(), THEME_VERSION);
}

/**
 * Enqueue styles for front-end.
 * @param bool $debug Whether to enable loading uncompressed/debugging assets. Default false.
 * @return void
 */
function enqueue_styles()
{
	wp_enqueue_style('main', THEME_URL . "/assets/dist/css/main.css", array(), THEME_VERSION);

	if (ACTIVATE_FLICKITY) :
		wp_enqueue_style('flickity', THEME_URL . "/libraries/flickity/flickity.min.css", array(), THEME_VERSION);
		wp_enqueue_style('flickity-fade', THEME_URL . "/libraries/flickity/flickity-fade.min.css", array(), THEME_VERSION);
	endif;

	wp_deregister_style('wp-block-library');
	wp_deregister_style('wp-block-library-theme');
	wp_deregister_style('wc-block-style');
}

/**
 * Enqueue scripts for front-end.
 * @param bool $debug Whether to enable loading uncompressed/debugging assets. Default false.
 * @return void
 */
function enqueue_scripts()
{
	wp_register_script('main', THEME_URL . "/assets/dist/js/bundle.js", array('jquery'), THEME_VERSION, true);
	wp_localize_script(
		'main',
		'options',
		array(
			'activate_prx_marquee' => ACTIVATE_PRX_MARQUEE,
			'activate_prx_scroll' => ACTIVATE_PRX_SCROLL,
			'activate_flickity' => ACTIVATE_FLICKITY,
		)
	);
	wp_enqueue_script('main');

	if (ACTIVATE_FLICKITY) :
		wp_enqueue_script('flickity', THEME_URL . "/libraries/flickity/flickity.min.js", array(), THEME_VERSION);
		wp_enqueue_script('flickity-fade', THEME_URL . "/libraries/flickity/flickity-fade.min.js", array(), THEME_VERSION);
	endif;

	//wp_enqueue_script('smooth-scroll', THEME_URL . "/libraries/smooth-scroll/smooth-scroll.js", array(), THEME_VERSION);
}

function after_setup_theme()
{
	// Add theme supports
	add_theme_support('title-tag');
	add_theme_support('post-thumbnails');
	add_theme_support('menus');

	add_image_size('lp_giant', 2560);
	add_image_size('lp_large', 1440);
	add_image_size('lp_medium', 720);
	add_image_size('lp_small', 360);
}

/**
 * Add favicon to WordPress back-end interface
 */
add_action('admin_head', function () {
	echo '<link href="' . THEME_URL . '/favicon.ico" rel="icon" type="image/x-icon">';
});

/**
 * Create the default ACF options pages
 */
function base_acf_options_pages()
{
	if (function_exists('acf_add_options_page')) {
		acf_add_options_page(array(
			'page_title'  => 'Options générales',
			'menu_title'  => 'Options générales',
			'menu_slug'   => 'options',
			'capability'  => 'edit_posts',
			'redirect'    => false
		));

		acf_add_options_sub_page(array(
			'page_title'  => 'Coordonnées',
			'menu_title'  => 'Coordonnées',
			'parent_slug' => 'options',
		));

		acf_add_options_sub_page(array(
			'page_title'  => 'Réseaux sociaux',
			'menu_title'  => 'Réseaux sociaux',
			'parent_slug' => 'options',
		));
	}
}
