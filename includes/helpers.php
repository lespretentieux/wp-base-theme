<?php

function sanitize_phone_format(string $string)
{
    /**
     * Keep only numbers
     */
    $string = preg_replace('/[^0-9]/', '', $string);

    /**
     * Keep only 10 numbers in string (removes 'Poste + ext')
     */
    $string = substr($string, 0, 10);

    return $string;
}

/**
 * Returns the "srcset" attribute from an ACF image object
 *
 * @param array $image ACF image object
 * 
 * @return string
 */
function get_srcset($image)
{
    $srcset = '';
    $srcset .= $image['sizes']['lp_giant'] . ' 2560w,';
    $srcset .= $image['sizes']['lp_large'] . ' 1440w,';
    $srcset .= $image['sizes']['lp_medium'] . ' 720w,';
    $srcset .= $image['sizes']['lp_small'] . ' 360w';
    return $srcset;
}

/**
 * Generates and returns the complete markup of an image from an ACF image object
 *
 * @param array $image ACF image object
 * @param string $sizes Sizes attribute for the image
 * @param string $classes Classes for the image
 * 
 * @return string
 */
function get_image($image, $sizes, $classes = '')
{
    $tag = '<img';
    $tag .= $classes ? ' class="' . $classes . '"' : '';
    $tag .= ' src="' . $image['sizes']['lp_giant'] . '"';
    $tag .= ' srcset="' . get_srcset($image) . '"';
    $tag .= ' sizes="' . $sizes . '"';
    $tag .= ' alt="' . $image['alt'] . '"';
    $tag .= '>';
    return $tag;
}

/**
 * Returns the permalink of the given path in the current language
 *
 * @param string $path The path
 * 
 * @return string
 */
function get_translated_permalink($path)
{
    return get_permalink(get_page_by_path($path));
}

/**
 * Returns a WordPress menu as an object
 *
 * @param array $elements The menu elements
 * @param int $parent_id The menu item's parent id
 * 
 * @return array
 */
function get_menu_object(array &$elements, $parent_id = 0)
{
    $branch = array();

    foreach ($elements as &$element) {
        if ($element->menu_item_parent == $parent_id) {
            $children = get_menu_object($elements, $element->ID);

            if ($children) {
                $element->wpse_children = $children;
            }

            $branch[$element->ID] = $element;
            unset($element);
        }
    }

    return $branch;
}
