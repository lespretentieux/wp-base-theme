<?php

namespace THEME_WP\Menus;

/**
 * Set up menus
 *
 * @return void
 */
function setup()
{
	$n = function ($function) {
		return __NAMESPACE__ . "\\$function";
	};

	register_nav_menus(array(
		'main_menu' => 'Main Menu',
		'secondary_menu' => 'Secondary Menu',
		'footer_menu' => 'Footer Menu',
	));
}
