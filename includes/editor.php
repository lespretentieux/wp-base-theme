<?php

namespace THEME_WP\Editor;

/**
 * Set up theme defaults and register supported WordPress features.
 *
 * @return void
 */
function setup()
{
    $n = function ($function) {
        return __NAMESPACE__ . "\\$function";
    };

    add_filter('mce_buttons_2', $n('my_mce_buttons_2'));
    add_filter('tiny_mce_before_init', $n('my_mce_before_init_insert_formats'));
    add_editor_style(THEME_URL . "/assets/dist/css/main.css");
}

/*
 * Callback function to filter the MCE settings
 */

// Callback function to insert 'styleselect' into the $buttons array
function my_mce_buttons_2($buttons)
{
    array_unshift($buttons, 'styleselect');
    return $buttons;
}

function my_mce_before_init_insert_formats($init_array)
{
    // Define the style_formats array
    $style_formats = array(
        array(
            'title' => 'Bouton',
            'inline' => 'a',
            'classes' => 'c-btn'
        ),
    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode($style_formats);
    return $init_array;
}
