<?php

namespace THEME_WP\Post_Types;

/**
 * Set up Custom Post Types
 * @link https://github.com/jjgrainger/wp-custom-post-type-class

 * @return void
 */
function setup()
{
	$n = function ($function) {
		return __NAMESPACE__ . "\\$function";
	};

	/**
	 * @example
	 * $books = new \CPT('book');
	 * $books->register_taxonomy('genre');
	 * $books->flush();
	 */
}
