<?php

get_header();

if (have_rows('page_headers')) :
    while (have_rows('page_headers')) : the_row();
        $moduleType = get_row_layout();
        $moduleTypeFilePath = sprintf(get_stylesheet_directory() . '/partials/headers/%s.php', $moduleType);
        if (file_exists($moduleTypeFilePath)) :
            include $moduleTypeFilePath;
        endif;
    endwhile;
endif;

if (have_rows('page_blocks')) :
    while (have_rows('page_blocks')) : the_row();
        $moduleType = get_row_layout();
        $moduleTypeFilePath = sprintf(get_stylesheet_directory() . '/partials/blocks/%s.php', $moduleType);
        if (file_exists($moduleTypeFilePath)) :
            include $moduleTypeFilePath;
        endif;
    endwhile;
endif;

get_footer();
