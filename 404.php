<?php get_header(); ?>

<main>
	<h1>Erreur 404 - page introuvable</h1>

	<h5>Désolé, il est impossible de trouver la page.</h5>
	<p>L'adresse que vous tentez d'atteindre est introuvable ou inaccessible en ce moment.</p>

	<a href="#" onclick="window.history.back(); return false;" class="c-btn">Retourner à la page précédente</a>
	<a href="<?php echo get_site_url(); ?>" class="c-btn">Aller à l'accueil</a>
</main>

<?php get_footer(); ?>