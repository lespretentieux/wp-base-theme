/**
 * PrxScroll v1.0.0
 *
 * Author: Les Prétentieux
 * https://www.lespretentieux.com/
 *
 * See {@link https://lespretentieux.slite.com/app/docs/D-hOyt8OBoQhIY}.
 *
 * */

"use strict";

export class PrxScroll {
	/**
	 * Init our class
	 *
	 * @param easing (float): easing value. Default to 0.1.
	 */
	constructor({ lerp = 0.1 } = {}) {
		this.easing = lerp;

		// Stores the scroll values
		this.scroll = {};

		this.isTouch = false;

		this.inViewElems = [];
		this.parallaxElems = [];
		this.anchorElems = [];

		// Add our event listeners
		window.addEventListener("wheel", (event) => this.wheelEvent(event), { passive: false });
		window.addEventListener("scroll", () => this.scrollEvent(), { passive: true });
		window.addEventListener("resize", () => this.resizeEvent(), { passive: true });

		this.init();
	}

	/**
	 * Initialize smooth scroll and add events
	 */
	init() {
		window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

		// Set ours scroll values to the current scroll value
		this.syncPos();

		// Get all our elements
		this.inViewElems = document.querySelectorAll("[data-scroll-in-view]");
		this.parallaxElems = document.querySelectorAll("[data-scroll-parallax]");
		this.anchorElems = document.querySelectorAll("[data-scroll-anchor]");

		// Check if device is touch
		if (this.isTouchDevice()) {
			this.isTouch = true;
		}

		// If device is not touch, initialize smooth scroll
		if (!this.isTouch) {
			requestAnimationFrame(() => this.tick());
		}

		this.anchorElems.forEach((anchorElem) => {
			anchorElem.addEventListener("click", (event) => this.anchorClick(event, anchorElem));
		});

		// Update in view and parallax elements
		this.updateInView();
		this.updateParallax();
	}

	/**
	 * Update our scroll related elements
	 */
	refresh() {
		// Empty all elements
		this.inViewElems = [];
		this.parallaxElems = [];

		// Get all our elements
		this.inViewElems = document.querySelectorAll("[data-scroll-in-view]");
		this.parallaxElems = document.querySelectorAll("[data-scroll-parallax]");

		// Update in view and parallax elements
		this.updateInView();
		this.updateParallax();
	}

	/**
	 * Update our scroll related events on scroll
	 */
	scrollEvent() {
		this.updateInView();
		this.updateParallax();
	}

	/**
	 * Update our scroll related events on resize
	 */
	resizeEvent() {
		this.updateInView();
		this.updateParallax();
	}

	/**
	 * Update scroll target on mouse wheel input
	 */
	wheelEvent(event) {
		var hoveredElems = document.querySelectorAll(":hover");
		var scrollableElems = 0;

		hoveredElems.forEach((hoveredElem) => {
			if (this.isScrollable(hoveredElem)) {
				scrollableElems++;
			}
		});

		// This means we're scrolling in the document (so if we were to scroll anywhere else, the default scroll would be taking place instead)
		if (scrollableElems < 2 && !this.isTouch) {
			event.preventDefault();
			var scrollSpeed = event.deltaY;
			this.scroll.target = this.capScrollPosition(this.scroll.target + scrollSpeed);
		}
	}

	/**
	 * Scroll to specified section when user clicks on a smooth anchor link
	 *
	 * @param anchorElem (node): the clicked anchor element
	 *
	 */
	anchorClick(event, anchorElem) {
		event.preventDefault();
		var href = anchorElem.getAttribute("href");
		this.scrollTo(href);
	}

	/**
	 * Tick 60 times per second with requestAnimationFrame
	 */
	tick() {
		// Only render when our scroll position is different from our scroll target (when moving)
		if (Math.round(this.scroll.pos) != Math.round(this.scroll.target)) {
			this.render();
		} else {
			// Sync scroll position when user stops scrolling
			this.syncPos();
		}

		requestAnimationFrame(() => this.tick());
	}

	/**
	 * Render/animate our scroll
	 */
	render() {
		this.scroll.pos = this.lerp(this.scroll.pos, this.scroll.target, this.easing);
		window.scroll(0, this.scroll.pos);
	}

	/**
	 * Scroll to a specified target or fixed pixel value
	 *
	 * @param target (string|int): selector to scroll to | fixed position to scroll to
	 * @param instant (bool): specify if the transition is instant or smooth
	 *
	 */
	scrollTo(target, instant = false) {
		var pos = 0;

		// If target is not a number, interpret it as a query selector
		if (isNaN(target)) {
			// Get position of target element
			var targetEl = document.querySelector(target);
			var rect = targetEl.getBoundingClientRect();
			pos = this.capScrollPosition(rect.top + window.scrollY);
		} else {
			pos = target;
		}

		pos = this.round(pos);

		// If device is touch, scroll to position instantly
		if (this.isTouch) {
			window.scroll(0, pos);
		} else {
			this.scroll.target = pos;

			if (instant) {
				this.scroll.pos = pos;
			}
		}
	}

	/**
	 * Update our parallax elements' values
	 */
	updateParallax() {
		this.parallaxElems.forEach((parallaxElem) => {
			var parallaxDirection = parallaxElem.getAttribute("data-parallax-direction") ? parallaxElem.getAttribute("data-parallax-direction") : "vertical";
			var parallaxSpeed = parallaxElem.getAttribute("data-parallax-speed") ? parallaxElem.getAttribute("data-parallax-speed") : 1;

			// Get progress of element on scroll (from 0 to 1)
			var progress = this.getProgress(parallaxElem);
			// Convert the scale to -0.5 to 0.5 so we reach 0 when element is centered in the viewport
			var scale = progress - 0.5;
			// Reverse and multiply scale to accentuate transform
			var transform = scale * -100;
			// Add our parallax speed multiplier
			transform = transform * parallaxSpeed;
			// Round our transform to 2 decimal places
			transform = this.round(transform);

			var transformValue;

			if (parallaxDirection == "vertical") {
				transformValue = "translate3d(0, " + transform + "px, 0)";
			} else {
				transformValue = "translate3d(" + transform + "px, 0, 0)";
			}

			parallaxElem.style.transform = transformValue;
		});
	}

	/**
	 * Update our in-view elements
	 */
	updateInView() {
		this.inViewElems.forEach((inViewElem) => {
			if (this.elementInViewport(inViewElem)) {
				inViewElem.classList.add("is-inview");
			}
		});
	}

	/**
	 * Sync the scroll position to the current scroll position of the window
	 */
	syncPos() {
		this.scroll.pos = this.round(window.pageYOffset);
		this.scroll.target = this.round(window.pageYOffset);
	}

	/**
	 * Get the progress of an element in the viewport (from 0 to 1)
	 *
	 * @param el (node): the element to return the progress of
	 *
	 */
	getProgress(el) {
		var rect = el.getBoundingClientRect();
		var numerator = (rect.top - document.documentElement.clientHeight) * -1;
		var denominator = rect.height + document.documentElement.clientHeight;
		var progress = Math.min(Math.max(numerator / denominator, 0), 1);
		return progress;
	}

	/**
	 * Keep scroll position between 0 and the end of the document
	 *
	 * @param pos (float): the scroll position to cap
	 *
	 */
	capScrollPosition(pos) {
		return Math.min(Math.max(pos, 0), document.documentElement.scrollHeight - document.documentElement.clientHeight);
	}

	/**
	 * Utility function to linear interpolate values
	 *
	 * @param start (float): value to lerp
	 * @param end (float): target value
	 * @param amount (float): easing
	 *
	 * @returns (float): lerped value
	 */
	lerp(start, end, amount) {
		return this.round((1 - amount) * start + amount * end);
	}

	/**
	 * Utility function to check of a given element is in the viewport
	 *
	 * @param el (node): element to check if in viewport
	 *
	 * @returns (bool): if element in viewport or not
	 */
	elementInViewport(el) {
		var rect = el.getBoundingClientRect();
		return rect.top - document.documentElement.clientHeight <= 0;
	}

	/**
	 * Utility function to check of the device is a touch device
	 *
	 * @returns (bool): if device is a touch device or not
	 */
	isTouchDevice() {
		return "ontouchstart" in window || navigator.maxTouchPoints > 0 || navigator.msMaxTouchPoints > 0;
	}

	/**
	 * Shortcut for rounding number to second decimal place
	 *
	 * @returns (number): rounded number to second decimal place
	 */
	round(number) {
		return Math.round((number + Number.EPSILON) * 100) / 100;
	}

	/**
	 * Utility function to check if element is scrollable
	 *
	 * @returns (bool): if element is scrollable or not
	 */
	isScrollable(el) {
		if (el == document.scrollingElement) {
			return el.scrollHeight > el.clientHeight;
		}
		return el.scrollHeight > el.clientHeight && ["scroll", "auto"].indexOf(getComputedStyle(el).overflowY) >= 0;
	}
}
